# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]
### Changed
- Updated dependencies:
    - cdc-io-0.53.2
    - org.junit-5.12.0


## [0.31.3] - 2025-01-03
### Changed
- Updated dependencies:
    - cdc-io-0.53.1
    - cdc-kernel-0.51.3
    - cdc-util-0.54.0
    - org.junit-5.11.4
    - org.apache.log4j-2.24.3


## [0.31.2] - 2024-09-28
### Changed
- Updated dependencies:
    - cdc-io-0.52.1
    - cdc-kernel-0.51.2
    - cdc-util-0.53.0
    - commons-cli-1.9.0
    - org.junit-5.11.1


## [0.31.1] - 2024-05-18
### Changed
- Updated dependencies:
    - cdc-io-0.52.0
    - cdc-kernel-0.51.1
    - cdc-util-0.52.1
    - commons-cli-1.7.0
    - org.apache.log4j-2.23.1
    - org.junit-5.10.2
    - javafx-22.0.1
- Updated maven plugins.


## [0.31.0] - 2024-01-01
### Changed
- Updated dependencies:
    - cdc-io-0.51.0
    - cdc-kernel-0.51.0
    - cdc-util-0.52.0
- Updated maven plugins.


## [0.30.0] - 2023-11-25
### Changed
- Moved to Java 17
- Updated dependencies:
    - cdc-io-0.50.0
    - cdc-kernel-0.50.0
    - cdc-util-0.50.0
    - javafx-21.0.1


## [0.20.15] - 2023-11-18
### Changed
- Updated dependencies:
    - cdc-io-0.27.3
    - cdc-kernel-0.23.2
    - cdc-util-0.33.2
    - commons-cli-1.6.0
    - org.junit-5.10.1


## [0.20.14] - 2023-10-21
### Changed
- Updated dependencies:
    - cdc-io-0.27.2
    - cdc-kernel-0.23.1
    - cdc-util-0.33.1
    - org.junit-5.10.0
- Updated maven plugins.
- Added maven enforcer plugin.
- Used `java.version` and `maven.version` properties in pom. 


## [0.20.13] - 2023-07-15
### Changed
- Updated dependencies:
    - cdc-io-0.27.0
    - cdc-kernel-0.22.1
    - cdc-util-0.33.0


## [0.20.12] - 2023-04-22
### Changed
- Updated dependencies:
    - cdc-kernel-0.21.4


## [0.20.11] - 2023-04-15
### Changed
- Updated dependencies:
    - cdc-io-0.26.0
    - cdc-kernel-0.21.3


## [0.20.10] - 2023-02-25
### Changed
- Updated dependencies:
    - cdc-io-0.25.1
    - cdc-kernel-0.21.2
    - cdc-util-0.31.0
    - org.apache.log4j-2.20.0


## [0.20.9] - 2023-01-28
### Changed
- Updated dependencies:
    - cdc-io-0.24.0
    - cdc-kernel-0.21.1
    - cdc-util-0.29.0
    - org.junit-5.9.2


## [0.20.8] - 2023-01-02
### Changed
- Updated dependencies:
    - cdc-io-0.23.2
    - cdc-kernel-0.21.0
    - cdc-util-0.28.2
    - javafx-19


## [0.20.7] - 2022-11-09
### Changed
- Updated dependencies:
    - cdc-io-0.23.1
    - cdc-kernel-0.20.7
    - cdc-util-0.28.1
    - org.apache.log4j-2.19.0
    - org.junit-5.9.1


## [0.20.6] - 2022-08-24
### Changed
- Updated dependencies:
    - cdc-io-0.23.0
    - cdc-kernel-0.20.6
    - cdc-util-0.28.0
    - javafx-18.0.2
    - org.junit-5.9.0


## [0.20.5] - 2022-07-07
### Changed
- Updated dependencies:
    - cdc-io-0.22.0
    - cdc-kernel-0.20.5
    - cdc-util-0.27.0
    - org.apache.log4j-2.18.0
    - org.junit-5.9.0-RC1


## [0.20.4] - 2022-06-18
### Changed
- Updated dependencies:
    - cdc-io-0.21.3
    - cdc-kernel-0.20.4
    - cdc-util-0.26.0


## [0.20.3] - 2022-05-19
### Changed
- Updated maven plugins
- Updated dependencies:
    - cdc-io-0.21.2
    - cdc-kernel-0.20.3
    - cdc-util-0.25.0


## [0.20.2] - 2022-03-11
### Changed
- Updated dependencies:
    - cdc-io-0.21.1
    - cdc-kernel-0.20.2
    - cdc-util-0.23.0
    - org.apache.log4j-2.17.2


## [0.20.1] - 2022-02-13
### Changed
- Updated dependencies:
    - cdc-io-0.21.0
    - cdc-kernel-0.20.1
    - cdc-util-0.20.0


## [0.20.0] - 2022-02-05
### Changed
- Updated maven plugins
- Upgraded to Java 11
- Updated dependencies:
    - cdc-io-0.20.0
    - cdc-kernel-0.20.0


## [0.13.2] - 2022-01-03
### Security
- Updated dependencies:
    - cdc-util-0.14.2
    - cdc-io-0.13.2
    - cdc-kernel-0.14.2
    - org.apache.log4j-2.17.1. #4


## [0.13.1] - 2021-12-28
### Security
- Updated dependencies:
    - cdc-util-0.14.1
    - cdc-io-0.13.1
    - cdc-kernel-0.14.1
    - org.apache.log4j-2.17.0. #4


## [0.13.0] - 2021-12-15
### Security
- Updated dependencies:
    - cdc-util-0.14.0
    - cdc-io-0.13.0
    - cdc-kernel-0.14.0
    - org.apache.log4j-2.16.0. #4
    - commons-cli-1.5.0


## [0.12.1] - 2021-10-02
### Changed
- Updated dependencies.


## [0.12.0] - 2021-07-23
### Added
- Created CHANGELOG.md
- Created SwingProgressMonitor. It supports undetermined progress.
- Created HtmlFrame that can be used to display generated files. #3

### Changed
- Updated dependencies.
- Removed cdc-issues dependency and moved corresponding code to cdc-issues.
- Improved ExceptionDialog. #2