package cdc.ui.labels;

import java.awt.Color;

public class Style {
    private boolean bold = false;
    private boolean italic = false;
    private boolean underline = false;
    private boolean strike = false;
    private Color fontColor = null;

    public Style setBold(boolean value) {
        this.bold = value;
        return this;
    }

    public boolean isBold() {
        return bold;
    }

    public Style setItalic(boolean value) {
        this.italic = value;
        return this;
    }

    public boolean isItalic() {
        return italic;
    }

    public Style setUnderline(boolean value) {
        this.underline = value;
        return this;
    }

    public boolean isUnderline() {
        return underline;
    }

    public Style setStrike(boolean value) {
        this.strike = value;
        return this;
    }

    public boolean isStrike() {
        return strike;
    }

    public Style setFonColor(Color value) {
        this.fontColor = value;
        return this;
    }

    public Color getFontColor() {
        return fontColor;
    }

    public boolean hasFontAttributes() {
        return fontColor != null;
    }

    public static void openHtml(StringBuilder builder) {
        builder.append("<html>");
    }

    public static void closeHtml(StringBuilder builder) {
        builder.append("</html>");
    }

    public void open(StringBuilder builder) {
        if (bold) {
            builder.append("<b>");
        }
        if (italic) {
            builder.append("<i>");
        }
        if (underline) {
            builder.append("<u>");
        }
        if (strike) {
            builder.append("<strike>");
        }
        if (hasFontAttributes()) {
            builder.append("<font");
            if (getFontColor() != null) {
                builder.append("color=\"#" + Integer.toHexString(fontColor.getRGB() & 0xFFFFFF) + "\"");
            }
            builder.append(">");
        }
    }

    public void close(StringBuilder builder) {
        if (hasFontAttributes()) {
            builder.append("</font>");
        }
        if (strike) {
            builder.append("</strike>");
        }
        if (underline) {
            builder.append("</u>");
        }
        if (italic) {
            builder.append("</i>");
        }
        if (bold) {
            builder.append("</b>");
        }
    }

    public void append(StringBuilder builder,
                       String text,
                       boolean html) {
        if (html) {
            openHtml(builder);
        }
        open(builder);
        // TODO escape text !
        builder.append(text);
        close(builder);
        if (html) {
            closeHtml(builder);
        }
    }

    public String toString(String text,
                           boolean html) {
        final StringBuilder builder = new StringBuilder();
        append(builder, text, html);
        return builder.toString();
    }
}