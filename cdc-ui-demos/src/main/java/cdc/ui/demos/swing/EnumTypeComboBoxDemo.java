package cdc.ui.demos.swing;

import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.enums.AbstractForestDynamicEnum;
import cdc.enums.AbstractListDynamicEnum;
import cdc.ui.swing.enums.EnumTypeComboBoxModel;

public final class EnumTypeComboBoxDemo extends JFrame {
    private static final Logger LOGGER = LogManager.getLogger(EnumTypeComboBoxDemo.class);
    private static final long serialVersionUID = 1L;

    public static class List1 extends AbstractListDynamicEnum<List1> {
        public static final Support<List1> SUPPORT = support(List1.class,
                                                             List1::new);

        protected List1(String name) {
            super(name);
        }
    }

    public static class Tree1 extends AbstractForestDynamicEnum<Tree1> {
        public static final Support<Tree1> SUPPORT = support(Tree1.class,
                                                             Tree1::new);

        protected Tree1(Tree1 parent,
                        String name) {
            super(parent, name);
        }
    }

    private EnumTypeComboBoxDemo() {
        super();
        setTitle(getClass().getCanonicalName());
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        List1.SUPPORT.findOrCreate("A");
        List1.SUPPORT.findOrCreate("C");
        List1.SUPPORT.findOrCreate("B");

        Tree1.SUPPORT.findOrCreate("A");
        Tree1.SUPPORT.findOrCreate("A/B");
        Tree1.SUPPORT.findOrCreate("B/B");
        Tree1.SUPPORT.findOrCreate("B/A");

        final JPanel wPanel = new JPanel();
        getContentPane().add(wPanel);

        // List
        wPanel.add(new JLabel("List"));
        final ComboBoxModel<List1> modelList = EnumTypeComboBoxModel.create(List1.SUPPORT)
                                                                    .withNullable(true);
        final JComboBox<List1> wComboBoxList = new JComboBox<>(modelList);
        wComboBoxList.addActionListener(LOGGER::info);
        wComboBoxList.setEditable(true);
        wPanel.add(wComboBoxList);

        // Tree
        wPanel.add(new JLabel("Tree"));
        final ComboBoxModel<Tree1> modelTree = EnumTypeComboBoxModel.create(Tree1.SUPPORT)
                                                                    .withNullable(true);
        final JComboBox<Tree1> wComboBoxTree = new JComboBox<>(modelTree);
        wComboBoxTree.addActionListener(LOGGER::info);
        wPanel.add(wComboBoxTree);
    }
}