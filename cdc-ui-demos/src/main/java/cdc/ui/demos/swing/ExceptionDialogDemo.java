package cdc.ui.demos.swing;

import java.io.File;
import java.io.IOException;

import cdc.ui.swing.app.ExceptionDialog;

public class ExceptionDialogDemo {
    public static void main(String[] args) {
        ExceptionDialog.showExceptionDialog(null,
                                            new IOException(),
                                            "Message",
                                            new File("target"));
    }
}