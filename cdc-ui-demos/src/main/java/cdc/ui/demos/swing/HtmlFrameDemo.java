package cdc.ui.demos.swing;

import java.io.File;

import javax.swing.WindowConstants;

import cdc.ui.swing.widgets.HtmlFrame;

public class HtmlFrameDemo {
    public static void main(String[] args) {
        final HtmlFrame frame = new HtmlFrame(null);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        frame.setTitle(HtmlFrameDemo.class.getSimpleName());
        frame.getHtmlPanel()
             .append("Hello")
             .append("<br>Hello<br>")
             .append("<ul>")
             .append("<li>")
             .href(new File("src/main/java/cdc/ui/demos/swing/HtmlFrameDemo.java"), "HtmlFrameDemo.java")
             .append("<li>")
             .href(new File("src/main/java/cdc/ui/demos/swing/HtmlFrameDemo"), "Error")
             .append("</ul>")
             .build();
        frame.pack();
        frame.setVisible(true);
    }
}