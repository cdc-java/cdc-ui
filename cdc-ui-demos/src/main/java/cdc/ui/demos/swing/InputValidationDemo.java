package cdc.ui.demos.swing;

import java.awt.Dimension;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.ui.swing.props.PropertiesPanel;

public final class InputValidationDemo extends JFrame {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LogManager.getLogger(InputValidationDemo.class);

    private InputValidationDemo() {
        super();
        setTitle(getClass().getSimpleName());
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new Dimension(200, 10));
        final PropertiesPanel wProps = new PropertiesPanel();
        add(wProps);
        wProps.addProperty("P1", new JTextField(), true);
        wProps.addProperty("P2", new JTextField(), true);
        wProps.addFiller();

        for (int index = 0; index < wProps.getPropertiesCount(); index++) {
            wProps.setLabelToolTipTextAt(index, "Name tooltip of " + wProps.getNameAt(index));
            wProps.setPropertyToolTipTextAt(index, "Property tooltip of " + wProps.getNameAt(index));
        }

        final Verifier verifier = new Verifier();
        final Validator validator = new Validator();
        wProps.getPropertyAt(0).setInputVerifier(verifier);
        ((JTextField) wProps.getPropertyAt(0)).getDocument().addDocumentListener(validator);
        // wProps.getPropertyAt(1).setInputVerifier(verifier);
        ((JTextField) wProps.getPropertyAt(1)).getDocument().addDocumentListener(validator);

        pack();
    }

    private static class Verifier extends InputVerifier {
        @Override
        public boolean verify(JComponent input) {
            LOGGER.info("verify({})", ((JTextField) input).getText());
            return ((JTextField) input).getText().equals("Hello");
        }
    }

    private static class Validator implements DocumentListener {
        @Override
        public void insertUpdate(DocumentEvent e) {
            LOGGER.info("insertUpdate({})", e.getType());
            try {
                LOGGER.info("text: '{}'", e.getDocument().getText(0, e.getDocument().getLength()));
            } catch (final BadLocationException e1) {
                LOGGER.catching(e1);
            }
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            LOGGER.info("removeUpdate({})", e.getType());
            try {
                LOGGER.info("text: '{}'", e.getDocument().getText(0, e.getDocument().getLength()));
            } catch (final BadLocationException e1) {
                LOGGER.catching(e1);
            }
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            LOGGER.info("changedUpdate({})", e.getType());
            try {
                LOGGER.info("text: '{}'", e.getDocument().getText(0, e.getDocument().getLength()));
            } catch (final BadLocationException e1) {
                LOGGER.catching(e1);
            }
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            final InputValidationDemo frame = new InputValidationDemo();
            frame.setVisible(true);
        });
    }
}