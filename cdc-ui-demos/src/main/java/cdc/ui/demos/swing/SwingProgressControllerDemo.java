package cdc.ui.demos.swing;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.io.txt.LinesHandler;
import cdc.io.txt.LinesParser;
import cdc.io.utils.ProgressControllerInputStream;
import cdc.ui.swing.progress.SwingProgressController;
import cdc.util.events.ProgressSupplier;
import cdc.util.function.Evaluation;
import cdc.util.time.Chronometer;

public final class SwingProgressControllerDemo {
    static final Logger LOGGER = LogManager.getLogger(SwingProgressControllerDemo.class);

    private SwingProgressControllerDemo() {
    }

    private static File generate(int lines) throws IOException {
        final Chronometer chrono = new Chronometer();
        chrono.start();
        final File file = new File("target/demo-" + lines + ".txt");
        final SwingProgressController controller = new SwingProgressController(null, "Generate " + file);
        controller.setMillisToDecideToPopup(100);
        controller.setMillisToPopup(500);
        final ProgressSupplier progress = new ProgressSupplier(controller);
        LOGGER.info("Generate {}", file);
        progress.reset(lines + 1L, "Writing");
        try (PrintStream out = new PrintStream(new BufferedOutputStream(new FileOutputStream(file)))) {
            for (int index = 0; index < lines && !controller.isCancelled(); index++) {
                out.println("ABCDEGHIJKLMNOPQRSTUVWXYZABCDEGHIJKLMNOPQRSTUVWXYZABCDEGHIJKLMNOPQRSTUVWXYZABCDEGHIJKLMNOPQRSTUVWXYZ");
                progress.incrementValue();
            }
            progress.setDetail("Flushing");
            progress.setTotal(-1L);
            progress.fireProgress(true);
            out.flush();
        }
        progress.incrementValue();
        progress.close();
        chrono.suspend();
        LOGGER.info("Generated {} in {}", file, chrono);
        return file;
    }

    public static void main(String[] args) throws Exception {
        final LinesHandler handler = new LinesHandler() {
            @Override
            public Evaluation processLine(String line,
                                          int number) {
                return Evaluation.CONTINUE;
            }

            @Override
            public void processEnd() {
                // Ignore
            }

            @Override
            public void processBegin() {
                // Ignore
            }
        };

        final File file = generate(10_000_000);

        final SwingProgressController controller = new SwingProgressController(null, "Loading " + file);
        controller.setMillisToDecideToPopup(100);
        controller.setMillisToPopup(500);
        final Chronometer chrono = new Chronometer();
        chrono.start();
        LOGGER.info("Parse {}", file);
        try (final InputStream in = new ProgressControllerInputStream(file, controller)) {
            LinesParser.parse(in, file.getPath(), handler);
        }
        chrono.suspend();
        LOGGER.info("Parsed {} in {}", file, chrono);
    }
}