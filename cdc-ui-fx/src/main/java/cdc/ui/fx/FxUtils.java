package cdc.ui.fx;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.files.Resources;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public final class FxUtils {
    private static final Logger LOGGER = LogManager.getLogger(FxUtils.class);

    private static final ThreadLocal<List<Image>> APPLICATION_IMAGES = ThreadLocal.withInitial(() -> {
        final List<Image> result = new ArrayList<>();
        result.add(load("cdc/ui/images/cdc-16.png"));
        result.add(load("cdc/ui/images/cdc-24.png"));
        result.add(load("cdc/ui/images/cdc-32.png"));
        result.add(load("cdc/ui/images/cdc-48.png"));
        return result;
    });

    private FxUtils() {
    }

    public static List<Image> getApplicationImages() {
        return APPLICATION_IMAGES.get();
    }

    public static Image load(String path) {
        LOGGER.debug("load({})", path);
        final InputStream is = Resources.getResourceAsStream(path);
        if (is != null) {
            return new Image(is);
        } else {
            LOGGER.warn("load({}) FAILED", path);
            return null;
        }
    }

    public static Text toText(String text) {
        return new Text(text);
    }

    public static Text toBoldText(String text) {
        final Text w = new Text(text);
        final Font font = w.getFont();
        w.setFont(Font.font(font.getName(), FontWeight.BOLD, font.getSize()));
        return w;
    }

    public static void addMenuItem(Menu menu,
                                   String text,
                                   EventHandler<ActionEvent> onAction) {
        final MenuItem wMenuItem = new MenuItem(text);
        wMenuItem.setOnAction(onAction);
        menu.getItems().add(wMenuItem);
    }

    public static void expandAll(TreeItem<?> item) {
        if (item != null && !item.isLeaf()) {
            item.setExpanded(true);
            for (final TreeItem<?> child : item.getChildren()) {
                expandAll(child);
            }
        }
    }
}