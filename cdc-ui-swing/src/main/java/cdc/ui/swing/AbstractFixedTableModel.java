package cdc.ui.swing;

import javax.swing.table.AbstractTableModel;

import cdc.util.lang.Checks;

public abstract class AbstractFixedTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 1L;
    private final String[] columnNames;

    protected AbstractFixedTableModel(String... columnNames) {
        this.columnNames = columnNames.clone();
    }

    @Override
    public final int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public final String getColumnName(int column) {
        Checks.isInRange(column, "column", 0, columnNames.length - 1);
        return columnNames[column];
    }
}