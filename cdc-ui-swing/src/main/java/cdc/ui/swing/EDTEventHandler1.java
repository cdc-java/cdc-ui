package cdc.ui.swing;

import javax.swing.SwingUtilities;

/**
 * Utility class used to make sure a 1-arg event is processed in EDT.
 *
 * @author Damien Carbonne
 *
 * @param <A> The arg type.
 */
public abstract class EDTEventHandler1<A> {
    private class Handler implements Runnable {
        private final A arg;

        public Handler(A arg) {
            this.arg = arg;
        }

        @Override
        public void run() {
            processInEDT(arg);
        }
    }

    /**
     * Requires the processing of an event in EDT.
     * <p>
     * If current thread is EDT, then the event is immediately processed.
     * Otherwise is is queued in order to be processed later in EDT.
     *
     * @param arg The argument to process.
     */
    public void process(A arg) {
        if (SwingUtilities.isEventDispatchThread()) {
            processInEDT(arg);
        } else {
            SwingUtilities.invokeLater(new Handler(arg));
        }
    }

    /**
     * Called in EDT.
     *
     * @param arg The event to process.
     */
    protected abstract void processInEDT(A arg);
}