package cdc.ui.swing;

import javax.swing.SwingUtilities;

/**
 * Utility class used to make sure a 2-arg event is processed in EDT.
 *
 * @author Damien Carbonne
 *
 * @param <A> The first argument type.
 * @param <B> The second argument type.
 */
public abstract class EDTEventHandler2<A, B> {
    private class Handler implements Runnable {
        private final A arg0;
        private final B arg1;

        public Handler(A arg0,
                       B arg1) {
            this.arg0 = arg0;
            this.arg1 = arg1;
        }

        @Override
        public void run() {
            processInEDT(arg0, arg1);
        }
    }

    /**
     * Requires the processing of an event in EDT.
     * <p>
     * If current thread is EDT, then the event is immediately processed.
     * Otherwise is is queued in order to be processed later in EDT.
     *
     * @param arg0 The first argument.
     * @param arg1 The second argument.
     */
    public void process(A arg0,
                        B arg1) {
        if (SwingUtilities.isEventDispatchThread()) {
            processInEDT(arg0, arg1);
        } else {
            SwingUtilities.invokeLater(new Handler(arg0, arg1));
        }
    }

    /**
     * Called in EDT.
     *
     * @param arg0 The first argument.
     * @param arg1 The second argument.
     */
    protected abstract void processInEDT(A arg0,
                                         B arg1);
}