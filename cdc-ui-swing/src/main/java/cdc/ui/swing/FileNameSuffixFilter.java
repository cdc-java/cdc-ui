package cdc.ui.swing;

import java.io.File;
import java.util.Locale;

import javax.swing.filechooser.FileFilter;

public class FileNameSuffixFilter extends FileFilter {
    // Description of this filter.
    private final String description;
    // Known suffixes.
    private final String[] suffixes;
    // Cached suffixes
    private final String[] lowerCaseSuffixes;

    public FileNameSuffixFilter(String description,
                                String... suffixes) {
        if (suffixes == null || suffixes.length == 0) {
            throw new IllegalArgumentException("Suffixes must be non-null and not empty");
        }
        this.description = description;
        this.suffixes = new String[suffixes.length];
        this.lowerCaseSuffixes = new String[suffixes.length];
        for (int i = 0; i < suffixes.length; i++) {
            if (suffixes[i] == null || suffixes[i].length() == 0) {
                throw new IllegalArgumentException("Each suffix must be non-null and not empty");
            }
            this.suffixes[i] = suffixes[i];
            lowerCaseSuffixes[i] = suffixes[i].toLowerCase(Locale.ENGLISH);
        }
    }

    @Override
    public boolean accept(File f) {
        if (f != null) {
            if (f.isDirectory()) {
                return true;
            }
            final String filename = f.getName();
            final String lcfilename = filename.toLowerCase(Locale.ENGLISH);
            for (final String suffix : lowerCaseSuffixes) {
                if (lcfilename.endsWith(suffix)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public String[] getSuffixes() {
        return suffixes.clone();
    }

    @Override
    public String toString() {
        return super.toString() + "[description=" + getDescription() +
                " suffixes=" + java.util.Arrays.asList(getSuffixes()) + "]";
    }
}