package cdc.ui.swing;

import java.awt.Font;
import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.TableColumnModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.files.Resources;
import cdc.util.lang.Checks;

public final class SwingUtils {
    private static final Logger LOGGER = LogManager.getLogger(SwingUtils.class);
    private static Image applicationImage = null;

    private SwingUtils() {
    }

    public static void setPlainFont(JLabel c) {
        c.setFont(c.getFont().deriveFont(Font.PLAIN));
    }

    public static void setApplicationImage(Image image) {
        if (applicationImage == null) {
            applicationImage = image;
        } else {
            LOGGER.warn("Application image was already set");
        }
    }

    public static void setApplicationImage(URL url) {
        try {
            setApplicationImage(ImageIO.read(url));
        } catch (final IOException e) {
            LOGGER.catching(e);
        }
    }

    public static Image getApplicationImage() {
        if (applicationImage == null) {
            final URL url = Resources.getResource("cdc/ui/images/cdc-32.png");
            if (url != null) {
                setApplicationImage(url);
            } else {
                LOGGER.error("Failed to find the application image");
            }
        }
        return applicationImage;
    }

    public static Icon loadIcon(String path) {
        LOGGER.debug("loadIcon({})", path);
        final URL url = Resources.getResource(path);
        if (url != null) {
            return new ImageIcon(url);
        } else {
            LOGGER.warn("loadIcon({}) FAILED", path);
            return null;
        }
    }

    public static void setColumnWidths(JTable table,
                                       int... widths) {
        final TableColumnModel columnModel = table.getColumnModel();
        for (int i = 0; i < widths.length; i++) {
            if (i < columnModel.getColumnCount()) {
                columnModel.getColumn(i).setPreferredWidth(widths[i]);
            } else {
                break;
            }
        }
    }

    public static void assertInEDT(String message) {
        Checks.assertTrue(SwingUtilities.isEventDispatchThread(), "Not in EDT ({})", message);
    }

    public static void processInEDT(Runnable runnable) {
        if (SwingUtilities.isEventDispatchThread()) {
            runnable.run();
        } else {
            SwingUtilities.invokeLater(runnable::run);
        }
    }

    public static <T> void processInEDT(Consumer<T> consumer,
                                        T arg) {
        if (SwingUtilities.isEventDispatchThread()) {
            consumer.accept(arg);
        } else {
            SwingUtilities.invokeLater(() -> consumer.accept(arg));
        }
    }

    public static <T, U> void processInEDT(BiConsumer<T, U> consumer,
                                           T arg1,
                                           U arg2) {
        if (SwingUtilities.isEventDispatchThread()) {
            consumer.accept(arg1, arg2);
        } else {
            SwingUtilities.invokeLater(() -> consumer.accept(arg1, arg2));
        }
    }
}