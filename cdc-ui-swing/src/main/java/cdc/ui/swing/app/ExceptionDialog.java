package cdc.ui.swing.app;

import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.time.Instant;

import javax.swing.JOptionPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.app.AppInfo;
import cdc.app.AppInfoIo;

public final class ExceptionDialog {
    private static final Logger LOGGER = LogManager.getLogger(ExceptionDialog.class);

    private ExceptionDialog() {
    }

    public static void showExceptionDialog(Component parentComponent,
                                           Throwable exception,
                                           String message,
                                           File outputDir) {
        final Instant now = Instant.now();
        final File file = outputDir == null
                ? null
                : AppInfoIo.generateJsonExceptionFile(outputDir, now);
        final StringBuilder builder = new StringBuilder();
        if (message != null) {
            builder.append(message).append("\n");
        }
        builder.append(exception.getClass().getSimpleName());
        if (exception.getMessage() != null) {
            builder.append(" (")
                   .append(exception.getMessage())
                   .append(")");
        }
        builder.append(" occurred.");
        if (file != null) {
            builder.append("\nSave exception traces to '" + file + "'?");
        }
        final int answer = JOptionPane.showConfirmDialog(parentComponent,
                                                         builder.toString(),
                                                         exception.getClass().getSimpleName(),
                                                         JOptionPane.OK_CANCEL_OPTION,
                                                         JOptionPane.ERROR_MESSAGE);

        if (answer == JOptionPane.OK_OPTION) {
            final AppInfo info = AppInfo.builder()
                                        .stacktrace(exception)
                                        .timestamp(now)
                                        .build();
            if (file != null) {
                try {
                    LOGGER.info("Generate '{}'", file);
                    AppInfoIo.saveAsJson(info, file);
                    LOGGER.info("Generated '{}'", file);
                } catch (final IOException e) {
                    LOGGER.catching(e);
                }
            }
        }
    }
}