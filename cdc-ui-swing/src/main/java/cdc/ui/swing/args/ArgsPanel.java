package cdc.ui.swing.args;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import cdc.args.Args;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;

public class ArgsPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private final JTable wTable;
    private final ArgsTableModel model = new ArgsTableModel();

    public ArgsPanel() {
        super();
        setLayout(new GridBagLayout());

        wTable = new JTable(model);
        wTable.setDefaultRenderer(FormalArg.class, new FormalArgCellRenderer());
        // wTable.setDefaultRenderer(Object.class, new SwitchCellRenderer(model));

        // Currently values are edited as Strings.
        // They are automatically converted to the appropriate type.
        // TODO allow the use of a specialized editor for each argument.

        final JScrollPane wScrollPane = new JScrollPane();
        wScrollPane.setViewportView(wTable);
        {
            final GridBagConstraints gbc = new GridBagConstraints();
            gbc.fill = GridBagConstraints.BOTH;
            gbc.gridx = 0;
            gbc.gridy = 0;
            gbc.weightx = 1.0;
            gbc.weighty = 1.0;
            add(wScrollPane, gbc);
        }
    }

    public final void setFormalArgs(FormalArgs fargs) {
        model.setFormalArgs(fargs);
    }

    public final FormalArgs getFormalArgs() {
        return model.getFormalArgs();
    }

    public final void setArgs(Args args) {
        model.setArgs(args);
    }

    public final Args getArgs() {
        return model.getArgs();
    }
}