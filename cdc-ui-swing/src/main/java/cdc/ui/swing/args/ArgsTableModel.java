package cdc.ui.swing.args;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.args.Arg;
import cdc.args.Args;
import cdc.args.FormalArg;
import cdc.args.FormalArgs;
import cdc.converters.Converters;
import cdc.ui.swing.AbstractFixedTableModel;
import cdc.util.lang.Checks;
import cdc.util.lang.UnexpectedValueException;

public class ArgsTableModel extends AbstractFixedTableModel {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LogManager.getLogger(ArgsTableModel.class);
    private FormalArgs fargs = FormalArgs.NO_FARGS;
    private Args.Builder builder = Args.builder();

    public ArgsTableModel() {
        super("Name", "Value");
    }

    public void setFormalArgs(FormalArgs fargs) {
        Checks.isNotNull(fargs, "fargs");
        this.fargs = fargs;
        this.builder = Args.builder(fargs);
        fireTableDataChanged();
    }

    public FormalArgs getFormalArgs() {
        return fargs;
    }

    public void setArgs(Args args) {
        builder.clear();
        builder.args(args);
        fireTableDataChanged();
    }

    public Args getArgs() {
        return builder.build();
    }

    @Override
    public int getRowCount() {
        return fargs.size();
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
        case 0:
            return FormalArg.class;
        case 1:
            return Object.class;
        default:
            throw new UnexpectedValueException("Unexpected column " + columnIndex);
        }
    }

    @Override
    public final Object getValueAt(int rowIndex,
                                   int columnIndex) {
        switch (columnIndex) {
        case 0:
            return fargs.getArg(rowIndex);
        case 1:
            final Arg arg = builder.getArg(fargs.getArg(rowIndex).getName());
            return arg == null ? null : arg.getValue();
        default:
            return null;
        }
    }

    @Override
    public void setValueAt(Object value,
                           int rowIndex,
                           int columnIndex) {
        if (columnIndex == 1) {
            final FormalArg<?> farg = fargs.getArg(rowIndex);
            if (farg.isCompliantWith(value)) {
                builder.argRaw(farg, value);
            } else {
                try {
                    final Object tmp = Converters.convertRaw(value, farg.getType());
                    builder.argRaw(farg, tmp);
                } catch (final RuntimeException e) {
                    LOGGER.catching(e);
                }
            }
            fireTableCellUpdated(rowIndex, columnIndex);
        }
    }
}