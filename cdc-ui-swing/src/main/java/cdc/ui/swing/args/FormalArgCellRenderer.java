package cdc.ui.swing.args;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import cdc.args.FormalArg;

public class FormalArgCellRenderer implements TableCellRenderer {
    private final JLabel wLabel = new JLabel();

    @Override
    public Component getTableCellRendererComponent(JTable table,
                                                   Object value,
                                                   boolean isSelected,
                                                   boolean hasFocus,
                                                   int row,
                                                   int column) {
        final FormalArg<?> farg = (FormalArg<?>) value;
        if (farg == null) {
            wLabel.setText("");
        } else {
            wLabel.setText(farg.getName());
        }
        return wLabel;
    }
}