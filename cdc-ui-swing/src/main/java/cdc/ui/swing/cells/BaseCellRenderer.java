package cdc.ui.swing.cells;

import java.awt.Component;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.JTree;

public class BaseCellRenderer<C, D extends JComponent> extends CellHelper<C, D> implements CellRenderer<C> {

    public BaseCellRenderer(Class<C> contentClass,
                            D delegate) {
        super(contentClass,
              delegate);
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends C> list,
                                                  C value,
                                                  int index,
                                                  boolean isSelected,
                                                  boolean hasFocus) {
        final JList.DropLocation dropLocation = list.getDropLocation();
        final boolean isDropLocation = (dropLocation != null
                && !dropLocation.isInsert()
                && dropLocation.getIndex() == index);

        final ListCellParams params = new ListCellParams(CellUsage.RENDERER, list);
        params.setDelegate(delegate)
              .setHasFocus(hasFocus)
              .setEnabled(true)
              .setSelected(isSelected)
              .setViewRow(index)
              .setDropLocation(isDropLocation);

        configure(valueToItem(value), params);

        return delegate;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table,
                                                   Object value,
                                                   boolean isSelected,
                                                   boolean hasFocus,
                                                   int row,
                                                   int column) {
        final JTable.DropLocation dropLocation = table.getDropLocation();
        final boolean isDropLocation = (dropLocation != null
                && !dropLocation.isInsertRow()
                && !dropLocation.isInsertColumn()
                && dropLocation.getRow() == row
                && dropLocation.getColumn() == column);

        final TableCellParams params = new TableCellParams(CellUsage.RENDERER, table);
        params.setViewColumn(column)
              .setDelegate(delegate)
              .setHasFocus(hasFocus)
              .setEnabled(true)
              .setSelected(isSelected)
              .setViewRow(row)
              .setDropLocation(isDropLocation);

        // currentParams.modelColumn = table.convertColumnIndexToModel(column);
        // currentParams.modelRow = table.convertRowIndexToModel(row);

        configure(valueToItem(value), params);

        return delegate;
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree,
                                                  Object value,
                                                  boolean isSelected,
                                                  boolean isExpanded,
                                                  boolean isLeaf,
                                                  int row,
                                                  boolean hasFocus) {
        final JTree.DropLocation dropLocation = tree.getDropLocation();
        final boolean isDropLocation = (dropLocation != null
                && dropLocation.getChildIndex() == -1
                && tree.getRowForPath(dropLocation.getPath()) == row);

        final TreeCellParams params = new TreeCellParams(CellUsage.RENDERER, tree);
        params.setExpanded(isExpanded)
              .setLeaf(isLeaf)
              .setDelegate(delegate)
              .setHasFocus(hasFocus)
              .setEnabled(true)
              .setSelected(isSelected)
              .setViewRow(row)
              .setDropLocation(isDropLocation);

        configure(valueToItem(value), params);

        return delegate;
    }
}