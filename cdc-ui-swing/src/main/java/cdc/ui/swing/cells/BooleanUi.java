package cdc.ui.swing.cells;

import java.awt.Font;

import javax.swing.Icon;
import javax.swing.JTable;
import javax.swing.JToggleButton;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.UIResource;
import javax.swing.table.TableCellRenderer;

/**
 * Rendering/Edition of Booleans.
 * A text and an icon can be associated to true and false values.
 *
 * @author Damien Carbonne
 *
 */
public final class BooleanUi {
    private BooleanUi() {
    }

    public static final class Settings {
        private String trueText = "On";
        private String falseText = "Off";
        private Icon trueIcon = null;
        private Icon falseIcon = null;

        public String getTrueText() {
            return trueText;
        }

        public void setTrueText(String value) {
            this.trueText = value;
        }

        public String getFalseText() {
            return falseText;
        }

        public void setFalseText(String value) {
            this.falseText = value;
        }

        public Icon getTrueIcon() {
            return trueIcon;
        }

        public void setTrueIcon(Icon value) {
            this.trueIcon = value;
        }

        public Icon getFalseIcon() {
            return falseIcon;
        }

        public void setFalseIcon(Icon value) {
            this.falseIcon = value;
        }
    }

    public static final class Widget extends JToggleButton {
        private static final long serialVersionUID = 1L;
        private transient Settings settings = new Settings();

        public Widget(Settings settings) {
            setFont(getFont().deriveFont(Font.PLAIN));
            setSettings(settings);
        }

        public void setSettings(Settings settings) {
            if (settings != null) {
                this.settings = settings;
            }
        }

        public Settings getSettings() {
            return settings;
        }

        @Override
        public void setSelected(boolean selected) {
            super.setSelected(selected);
            setContentAreaFilled(false);
            setOpaque(true);
            if (selected) {
                setText(settings.getTrueText());
                setIcon(settings.getTrueIcon());
            } else {
                setText(settings.getFalseText());
                setIcon(settings.getFalseIcon());
            }
        }
    }

    public static class CellRenderer implements TableCellRenderer, UIResource {
        private static final Border NO_FOCUS_BORDER = new EmptyBorder(1, 1, 1, 1);

        protected Widget wComponent;

        public CellRenderer(Settings settings) {
            wComponent = new Widget(settings);
        }

        @Override
        public Widget getTableCellRendererComponent(JTable table,
                                                    Object value,
                                                    boolean isSelected,
                                                    boolean hasFocus,
                                                    int row,
                                                    int column) {
            if (isSelected) {
                wComponent.setForeground(table.getSelectionForeground());
                wComponent.setBackground(table.getSelectionBackground());
            } else {
                wComponent.setForeground(table.getForeground());
                wComponent.setBackground(table.getBackground());
            }
            wComponent.setSelected((value != null && ((Boolean) value).booleanValue()));

            if (hasFocus) {
                wComponent.setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
            } else {
                wComponent.setBorder(NO_FOCUS_BORDER);
            }

            return wComponent;
        }
    }

    public static class CellEditor extends CellEditorHelper<Widget> {
        private static final long serialVersionUID = 1L;

        public CellEditor(Settings settings) {
            wComponent = new Widget(settings);
            delegate = new EditorDelegate() {
                @Override
                public void setValue(Object value) {
                    boolean selected = false;
                    if (value instanceof Boolean) {
                        selected = ((Boolean) value).booleanValue();
                    }
                    wComponent.setSelected(selected);
                }

                @Override
                public Object getCellEditorValue() {
                    return Boolean.valueOf(wComponent.isSelected());
                }
            };
            wComponent.addActionListener(delegate);
            wComponent.setRequestFocusEnabled(false);
        }
    }
}