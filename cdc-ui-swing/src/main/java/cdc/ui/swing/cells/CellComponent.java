package cdc.ui.swing.cells;

import javax.swing.JComponent;

public interface CellComponent<C> {
    public Class<C> getContentClass();

    public JComponent getDelegate();

    public void configure(C value,
                          CellParams params);
}