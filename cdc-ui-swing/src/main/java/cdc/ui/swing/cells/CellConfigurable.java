package cdc.ui.swing.cells;

@FunctionalInterface
public interface CellConfigurable {
    public void configure(CellParams params);
}