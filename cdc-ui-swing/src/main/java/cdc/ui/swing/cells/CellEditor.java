package cdc.ui.swing.cells;

import javax.swing.table.TableCellEditor;
import javax.swing.tree.TreeCellEditor;

public interface CellEditor<C> extends CellComponent<C>, TableCellEditor, TreeCellEditor {
    // Empty
}