package cdc.ui.swing.cells;

import java.awt.Color;
import java.util.EnumMap;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.DefaultMutableTreeNode;

import cdc.util.lang.Checks;
import cdc.util.lang.UnexpectedValueException;

public class CellHelper<C, D extends JComponent> implements CellComponent<C> {
    private final Class<C> contentClass;
    protected D delegate;
    private final Map<SpecialColor, Color> colors = new EnumMap<>(SpecialColor.class);
    private CellParams params;

    protected CellHelper(Class<C> contentClass,
                         D delegate) {
        Checks.isNotNull(contentClass, "contentClass");
        this.contentClass = contentClass;
        this.delegate = delegate;
    }

    protected CellHelper(Class<C> contentClass) {
        this(contentClass, null);
    }

    @Override
    public final Class<C> getContentClass() {
        return contentClass;
    }

    public void setDelegate(D delegate) {
        this.delegate = delegate;
    }

    @Override
    public D getDelegate() {
        return delegate;
    }

    @Override
    public void configure(C value,
                          CellParams params) {
        this.params = params;
        if (delegate instanceof CellConfigurable) {
            ((CellConfigurable) delegate).configure(params);
        } else {
            final Color newBackground;
            final Color newForeground;
            if (params.isSelected()) {
                switch (params.getCellType()) {
                case LIST:
                    @SuppressWarnings("unchecked")
                    final JList<C> list = (JList<C>) params.getComponent();
                    newBackground = choose(SpecialColor.SELECTED_BG, list.getSelectionBackground());
                    newForeground = choose(SpecialColor.SELECTED_FG, list.getSelectionForeground());
                    break;
                case TABLE:
                    final JTable table = (JTable) params.getComponent();
                    newBackground = choose(SpecialColor.SELECTED_BG, table.getSelectionBackground());
                    newForeground = choose(SpecialColor.SELECTED_FG, table.getSelectionForeground());
                    break;
                case TREE:
                    final JTree tree = (JTree) params.getComponent();
                    newBackground = choose(SpecialColor.SELECTED_BG, tree.getBackground());
                    newForeground = choose(SpecialColor.SELECTED_FG, tree.getForeground());
                    break;
                default:
                    throw new UnexpectedValueException(params.getCellType());
                }
            } else {
                newBackground = choose(SpecialColor.NORMAL_BG, params.getComponent().getBackground());
                newForeground = choose(SpecialColor.SELECTED_FG, params.getComponent().getForeground());
            }
            if (delegate.getBackground() != newBackground) {
                delegate.setBackground(newBackground);
            }
            if (delegate.getForeground() != newForeground) {
                delegate.setForeground(newForeground);
            }

            if (params.isDropLocation()) {
                if (getColor(SpecialColor.DROP_LOCATION_BG) != null) {
                    delegate.setBackground(getColor(SpecialColor.DROP_LOCATION_BG));
                }
                if (getColor(SpecialColor.DROP_LOCATION_FG) != null) {
                    delegate.setForeground(getColor(SpecialColor.DROP_LOCATION_FG));
                }
            }
            if (delegate.getFont() != params.getComponent().getFont()) {
                delegate.setFont(params.getComponent().getFont());
            }
        }
    }

    public void setColor(SpecialColor kind,
                         Color color) {
        colors.put(kind, color);
    }

    public final Color getColor(SpecialColor kind) {
        return colors.get(kind);
    }

    public final CellParams getParams() {
        return params;
    }

    protected C valueToItem(Object value) {
        if (value == null) {
            return null;
        } else if (contentClass.isInstance(value)) {
            return contentClass.cast(value);
        } else if (value instanceof DefaultMutableTreeNode) {
            final DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
            return valueToItem(node.getUserObject());
        } else {
            return null;
        }
    }

    private Color choose(SpecialColor kind,
                         Color def) {
        return getColor(kind) == null ? def : getColor(kind);
    }

    protected void resetBorder() {
        delegate.setBorder(null);
    }

    protected void setBorder(Border border) {
        delegate.setBorder(new CompoundBorder(border, delegate.getBorder()));
    }

    protected void indent(int amount) {
        if (amount <= 0) {
            setBorder(null);
        } else {
            final EmptyBorder border = new EmptyBorder(1, amount, 1, 1);
            setBorder(border);
        }
    }

    // protected void adaptHeight() {
    // params.adaptHeight();
    // }
    //
    // private static class HeightAdapter implements Runnable {
    // private final CellParams params;
    //
    // public HeightAdapter(CellParams params) {
    // this.params.set(params);
    // }
    //
    // @Override
    // public void run() {
    // this.params.adaptHeight();
    // }
    // }
    //
    // protected void adaptHeightLater() {
    // SwingUtilities.invokeLater(new HeightAdapter(params));
    // }
}