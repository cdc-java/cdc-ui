package cdc.ui.swing.cells;

import javax.swing.JComponent;
import javax.swing.JTable;

public abstract class CellParams {
    private final CellUsage cellUsage;
    private final JComponent component;
    private JComponent delegate;
    private boolean isSelected;
    private boolean hasFocus;
    private boolean isDropLocation;
    private boolean isEnabled;
    private int viewRow;

    protected CellParams(CellUsage cellUsage,
                         JComponent component) {
        this.cellUsage = cellUsage;
        this.component = component;
    }

    public abstract CellType getCellType();

    public CellUsage getCellUsage() {
        return cellUsage;
    }

    public JComponent getComponent() {
        return component;
    }

    public final JComponent getDelegate() {
        return delegate;
    }

    public CellParams setDelegate(JComponent delegate) {
        this.delegate = delegate;
        return this;
    }

    public final boolean isSelected() {
        return isSelected;
    }

    public CellParams setSelected(boolean isSelected) {
        this.isSelected = isSelected;
        return this;
    }

    public final boolean hasFocus() {
        return hasFocus;
    }

    public CellParams setHasFocus(boolean hasFocus) {
        this.hasFocus = hasFocus;
        return this;
    }

    public final boolean isDropLocation() {
        return isDropLocation;
    }

    public CellParams setDropLocation(boolean isDropLocation) {
        this.isDropLocation = isDropLocation;
        return this;
    }

    public final boolean isEnabled() {
        return isEnabled;
    }

    public CellParams setEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
        return this;
    }

    public final int getViewRow() {
        return viewRow;
    }

    public CellParams setViewRow(int viewRow) {
        this.viewRow = viewRow;
        return this;
    }

    public void adaptHeight() {
        if (getCellType() == CellType.TABLE) {
            final JTable table = (JTable) component;
            if (viewRow < table.getRowCount()) {
                final int desiredHeight = delegate.getPreferredSize().height;
                if (table.getRowHeight(viewRow) != desiredHeight) {
                    table.setRowHeight(viewRow, desiredHeight);
                }
            }
        }
    }
}