package cdc.ui.swing.cells;

import javax.swing.ListCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.TreeCellRenderer;

public interface CellRenderer<C> extends CellComponent<C>, ListCellRenderer<C>, TableCellRenderer, TreeCellRenderer {
    // Ignore
}