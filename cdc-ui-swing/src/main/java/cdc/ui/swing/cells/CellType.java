package cdc.ui.swing.cells;

public enum CellType {
    LIST,
    TABLE,
    TREE
}