package cdc.ui.swing.cells;

/**
 * Enumeration of possible cell usages : RENDERER and EDITOR.
 * @author D. Carbonne
 *
 */
public enum CellUsage {
    RENDERER,
    EDITOR
}
