package cdc.ui.swing.cells;

import java.awt.Component;
import java.awt.Font;

import javax.swing.DefaultCellEditor;
import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.UIResource;
import javax.swing.table.TableCellRenderer;

public final class EnumUi {
    private EnumUi() {
    }

    public static class Settings<E extends Enum<E>> {
        private boolean nullable = false;

        public Settings() {
            super();
        }

        public boolean isNullable() {
            return nullable;
        }

        public void setNullable(boolean value) {
            this.nullable = value;
        }

        public String getText(E value) {
            return value == null ? "" : value.name();
        }

        public Icon getIcon(E value) {
            return null;
        }
    }

    public static class Widget<E extends Enum<E>> extends JComboBox<E> {
        private static final long serialVersionUID = 1L;
        private final Class<E> klass;
        protected final transient Settings<E> settings;

        private class Renderer extends JLabel implements ListCellRenderer<E> {
            private static final long serialVersionUID = 1L;

            public Renderer() {
                setOpaque(true);
                setFont(getFont().deriveFont(Font.PLAIN));
            }

            @Override
            public Component getListCellRendererComponent(JList<? extends E> list,
                                                          E value,
                                                          int index,
                                                          boolean isSelected,
                                                          boolean cellHasFocus) {
                if (settings != null) {
                    setText(settings.getText(value));
                    setIcon(settings.getIcon(value));
                } else {
                    setText(value == null ? "" : value.name());
                }
                if (isSelected) {
                    setForeground(list.getSelectionForeground());
                    setBackground(list.getSelectionBackground());
                } else {
                    setForeground(list.getForeground());
                    setBackground(list.getBackground());
                }
                return this;
            }
        }

        protected final Renderer renderer = new Renderer();

        public Widget(Class<E> klass,
                      Settings<E> settings) {
            super();
            if (klass == null) {
                throw new IllegalArgumentException("Null class");
            }
            this.klass = klass;
            this.settings = settings;
            for (final E value : klass.getEnumConstants()) {
                addItem(value);
            }
            setRenderer(renderer);
        }

        public Class<E> getEnumClass() {
            return klass;
        }
    }

    public static class CellRenderer<E extends Enum<E>> implements TableCellRenderer, UIResource {
        private static final Border NO_FOCUS_BORDER = new EmptyBorder(1, 1, 1, 1);
        protected Widget<E> wComponent;

        public CellRenderer(Class<E> klass,
                            Settings<E> settings) {
            wComponent = new Widget<>(klass, settings);
        }

        @Override
        public Widget<E> getTableCellRendererComponent(JTable table,
                                                       Object value,
                                                       boolean isSelected,
                                                       boolean hasFocus,
                                                       int row,
                                                       int column) {
            if (isSelected) {
                wComponent.setForeground(table.getSelectionForeground());
                wComponent.setBackground(table.getSelectionBackground());
            } else {
                wComponent.setForeground(table.getForeground());
                wComponent.setBackground(table.getBackground());
            }
            wComponent.setSelectedItem(value);
            if (hasFocus) {
                wComponent.setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
            } else {
                wComponent.setBorder(NO_FOCUS_BORDER);
            }

            return wComponent;
        }
    }

    public static class LabelCellRenderer<E extends Enum<E>> implements TableCellRenderer, UIResource {
        private static final Border NO_FOCUS_BORDER = new EmptyBorder(1, 1, 1, 1);
        protected JLabel wComponent;
        protected final Settings<E> settings;
        private final Class<E> klass;

        public LabelCellRenderer(Class<E> klass,
                                 Settings<E> settings) {
            wComponent = new JLabel();
            wComponent.setFont(wComponent.getFont().deriveFont(Font.PLAIN));
            wComponent.setOpaque(true);
            this.klass = klass;
            this.settings = settings;
        }

        @Override
        public JLabel getTableCellRendererComponent(JTable table,
                                                    Object value,
                                                    boolean isSelected,
                                                    boolean hasFocus,
                                                    int row,
                                                    int column) {
            if (isSelected) {
                wComponent.setForeground(table.getSelectionForeground());
                wComponent.setBackground(table.getSelectionBackground());
            } else {
                wComponent.setForeground(table.getForeground());
                wComponent.setBackground(table.getBackground());
            }
            wComponent.setText(settings.getText(klass.cast(value)));
            wComponent.setIcon(settings.getIcon(klass.cast(value)));
            if (hasFocus) {
                wComponent.setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
            } else {
                wComponent.setBorder(NO_FOCUS_BORDER);
            }

            return wComponent;
        }
    }

    public static class CellEditor<E extends Enum<E>> extends CellEditorHelper<Widget<E>> {
        private static final long serialVersionUID = 1L;

        public CellEditor(Class<E> klass,
                          Settings<E> settings) {
            wComponent = new Widget<>(klass, settings);
            delegate = new EditorDelegate() {
                @Override
                public void setValue(Object value) {
                    wComponent.setSelectedItem(value);
                }

                @Override
                public Object getCellEditorValue() {
                    return wComponent.getSelectedItem();
                }
            };
            wComponent.addItemListener(delegate);
            wComponent.setRequestFocusEnabled(false);
        }
    }

    public static class CellEditor2<E extends Enum<E>> extends DefaultCellEditor {
        private static final long serialVersionUID = 1L;

        public CellEditor2(Class<E> klass,
                           Settings<E> settings) {
            super(new Widget<>(klass, settings));
        }
    }
}