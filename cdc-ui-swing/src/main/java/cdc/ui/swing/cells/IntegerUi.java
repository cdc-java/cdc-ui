package cdc.ui.swing.cells;

import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.UIResource;
import javax.swing.table.TableCellRenderer;

/**
 * Rendering/Edition of Integers.
 *
 * @author Damien Carbonne
 *
 */
public final class IntegerUi {
    private IntegerUi() {
    }

    public static final class Settings {
        private String format = "%d";

        public String getFormat() {
            return format;
        }

        public void setFormat(String format) {
            this.format = format;
        }
    }

    public static final class Widget extends JLabel {
        private static final long serialVersionUID = 1L;
        private transient Settings settings = new Settings();

        public Widget(Settings settings) {
            setFont(getFont().deriveFont(Font.PLAIN));
            setSettings(settings);
            setOpaque(true);
            setHorizontalAlignment(SwingConstants.RIGHT);
        }

        public void setSettings(Settings settings) {
            if (settings != null) {
                this.settings = settings;
            }
        }

        public Settings getSettings() {
            return settings;
        }
    }

    public static class CellRenderer implements TableCellRenderer, UIResource {
        private static final Border NO_FOCUS_BORDER = new EmptyBorder(1, 1, 1, 1);

        protected Widget wComponent;

        public CellRenderer(Settings settings) {
            wComponent = new Widget(settings);
        }

        @Override
        public Widget getTableCellRendererComponent(JTable table,
                                                    Object value,
                                                    boolean isSelected,
                                                    boolean hasFocus,
                                                    int row,
                                                    int column) {
            if (isSelected) {
                wComponent.setForeground(table.getSelectionForeground());
                wComponent.setBackground(table.getSelectionBackground());
            } else {
                wComponent.setForeground(table.getForeground());
                wComponent.setBackground(table.getBackground());
            }

            if (hasFocus) {
                wComponent.setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
            } else {
                wComponent.setBorder(NO_FOCUS_BORDER);
            }

            if (value == null) {
                wComponent.setText("null");
            } else {
                wComponent.setText(String.format(wComponent.getSettings().getFormat(), (Integer) value));
            }

            return wComponent;
        }
    }
}