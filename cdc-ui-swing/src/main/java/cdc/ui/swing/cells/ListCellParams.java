package cdc.ui.swing.cells;

import javax.swing.JList;

public final class ListCellParams extends CellParams {
    public ListCellParams(CellUsage cellUsage,
                          JList<?> component) {
        super(cellUsage,
              component);
    }

    @Override
    public CellType getCellType() {
        return CellType.LIST;
    }

    @Override
    public JList<?> getComponent() {
        return (JList<?>) super.getComponent();
    }
}