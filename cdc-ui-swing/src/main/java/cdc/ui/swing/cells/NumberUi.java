package cdc.ui.swing.cells;

import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.UIResource;
import javax.swing.table.TableCellRenderer;

import cdc.util.lang.Checks;

/**
 * Rendering/Edition of Numbers.
 *
 * @author Damien Carbonne
 *
 */
public final class NumberUi {
    private NumberUi() {
    }

    public static final class Settings<N extends Number> {
        private final Class<N> numberClass;
        private String format = "";

        public Settings(Class<N> numberClass) {
            Checks.isNotNull(numberClass, "numberClass");

            this.numberClass = numberClass;
            if (numberClass.equals(Long.class)
                    || numberClass.equals(Integer.class)
                    || numberClass.equals(Short.class)
                    || numberClass.equals(Byte.class)) {
                this.format = "%,d";
            } else if (numberClass.equals(Double.class)
                    || numberClass.equals(Float.class)) {
                this.format = "%,f";
            } else {
                this.format = "";
            }
        }

        public Class<N> getNumberClass() {
            return numberClass;
        }

        public String getFormat() {
            return format;
        }

        public void setFormat(String format) {
            this.format = format;
        }
    }

    public static final class Widget<N extends Number> extends JLabel {
        private static final long serialVersionUID = 1L;
        private transient Settings<N> settings;

        public Widget(Settings<N> settings) {
            Checks.isNotNull(settings, "settings");

            setFont(getFont().deriveFont(Font.PLAIN));
            setSettings(settings);
            setOpaque(true);
            setHorizontalAlignment(SwingConstants.RIGHT);
        }

        public Class<N> getNumberClass() {
            return settings.getNumberClass();
        }

        public void setSettings(Settings<N> settings) {
            Checks.isNotNull(settings, "settings");
            this.settings = settings;
        }

        public Settings<N> getSettings() {
            return settings;
        }
    }

    public static class CellRenderer<N extends Number> implements TableCellRenderer, UIResource {
        private static final Border NO_FOCUS_BORDER = new EmptyBorder(1, 1, 1, 1);

        protected Widget<N> wComponent;

        public CellRenderer(Settings<N> settings) {
            wComponent = new Widget<>(settings);
        }

        public Class<N> getNumberClass() {
            return wComponent.getNumberClass();
        }

        @Override
        public Widget<N> getTableCellRendererComponent(JTable table,
                                                       Object value,
                                                       boolean isSelected,
                                                       boolean hasFocus,
                                                       int row,
                                                       int column) {
            if (isSelected) {
                wComponent.setForeground(table.getSelectionForeground());
                wComponent.setBackground(table.getSelectionBackground());
            } else {
                wComponent.setForeground(table.getForeground());
                wComponent.setBackground(table.getBackground());
            }

            if (hasFocus) {
                wComponent.setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
            } else {
                wComponent.setBorder(NO_FOCUS_BORDER);
            }

            if (value == null) {
                wComponent.setText("null");
            } else {
                wComponent.setText(String.format(wComponent.getSettings().getFormat(), value));
            }

            return wComponent;
        }
    }
}