package cdc.ui.swing.cells;

public enum SpecialColor {
    NORMAL_BG,
    NORMAL_FG,
    SELECTED_BG,
    SELECTED_FG,
    DROP_LOCATION_BG,
    DROP_LOCATION_FG
}