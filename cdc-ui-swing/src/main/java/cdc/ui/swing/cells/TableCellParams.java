package cdc.ui.swing.cells;

import javax.swing.JTable;

public final class TableCellParams extends CellParams {
    private int viewColumn;

    public TableCellParams(CellUsage cellUsage,
                           JTable component) {
        super(cellUsage,
              component);
    }

    @Override
    public CellType getCellType() {
        return CellType.TABLE;
    }

    @Override
    public JTable getComponent() {
        return (JTable) super.getComponent();
    }

    public int getViewColumn() {
        return viewColumn;
    }

    public TableCellParams setViewColumn(int viewColumn) {
        this.viewColumn = viewColumn;
        return this;
    }
}