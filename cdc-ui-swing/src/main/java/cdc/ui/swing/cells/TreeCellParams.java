package cdc.ui.swing.cells;

import javax.swing.JTree;

public final class TreeCellParams extends CellParams {
    private boolean isExpanded;
    private boolean isLeaf;

    public TreeCellParams(CellUsage cellUsage,
                          JTree component) {
        super(cellUsage,
              component);
    }

    @Override
    public CellType getCellType() {
        return CellType.TREE;
    }

    @Override
    public JTree getComponent() {
        return (JTree) super.getComponent();
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public TreeCellParams setExpanded(boolean isExpanded) {
        this.isExpanded = isExpanded;
        return this;
    }

    public boolean isLeaf() {
        return isLeaf;
    }

    public TreeCellParams setLeaf(boolean isLeaf) {
        this.isLeaf = isLeaf;
        return this;
    }
}