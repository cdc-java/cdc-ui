package cdc.ui.swing.enums;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

import javax.swing.DefaultComboBoxModel;

import cdc.enums.DagEventHandler;
import cdc.enums.EnumType;
import cdc.enums.EnumTypes;
import cdc.util.lang.Checks;
import cdc.util.lang.UnexpectedValueException;

public final class EnumTypeComboBoxModel<E extends EnumType<V>, V> extends DefaultComboBoxModel<V> {
    private static final long serialVersionUID = 1L;
    private final transient E enumType;
    private boolean nullable = false;
    private transient Predicate<V> predicate = v -> true;
    private transient Comparator<V> comparator;
    private final transient Comparator<V> defaultComparator;
    private final transient DagEventHandler handler;

    private EnumTypeComboBoxModel(E enumType) {
        Checks.isNotNull(enumType, "enumType");
        this.enumType = enumType;
        this.defaultComparator = EnumType.createQNameComparator(enumType);
        this.comparator = defaultComparator;
        refresh();

        if (enumType.isLocked()) {
            this.handler = null;
        } else {
            this.handler = event -> {
                switch (event.getEventType()) {
                case CREATED:
                case REMOVED:
                case REPARENTED:
                case CONTENT_CHANGED:
                case GLOBAL:
                    refresh();
                    break;
                case LOCKED:
                    enumType.removeEventHandler(getHandler());
                    break;
                default:
                    throw new UnexpectedValueException(event.getEventType());
                }
            };
            enumType.addEventHandler(handler);
        }
    }

    public static <E extends EnumType<V>, V> EnumTypeComboBoxModel<E, V> create(E enumType) {
        return new EnumTypeComboBoxModel<>(enumType);
    }

    public static <E extends Enum<E>> EnumTypeComboBoxModel<EnumType<E>, E> create(Class<E> enumClass) {
        return new EnumTypeComboBoxModel<>(EnumTypes.getEnumType(enumClass));
    }

    private DagEventHandler getHandler() {
        return handler;
    }

    public void refresh() {
        final List<V> values = new ArrayList<>();
        for (final V value : enumType.getValues()) {
            if (predicate.test(value)) {
                values.add(value);
            }
        }
        if (nullable && predicate.test(null)) {
            values.add(null);
        }
        Collections.sort(values, comparator);

        removeAllElements();
        for (final V value : values) {
            addElement(value);
        }
    }

    public E getEnumType() {
        return enumType;
    }

    public EnumTypeComboBoxModel<E, V> withNullable(boolean nullable) {
        if (nullable != this.nullable) {
            this.nullable = nullable;
            refresh();
        }
        return this;
    }

    public boolean isNullable() {
        return nullable;
    }

    public EnumTypeComboBoxModel<E, V> withPredicate(Predicate<V> predicate) {
        Checks.isNotNull(predicate, "predicate");
        this.predicate = predicate;
        refresh();
        return this;
    }

    public Predicate<V> getPredicate() {
        return predicate;
    }

    public EnumTypeComboBoxModel<E, V> withComparator(Comparator<V> comparator) {
        Checks.isNotNull(comparator, "comparator");
        this.comparator = comparator;
        refresh();
        return this;
    }

    public Comparator<V> getDefaultComparator() {
        return defaultComparator;
    }

    public Comparator<V> getComparator() {
        return comparator;
    }
}