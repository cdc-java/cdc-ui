package cdc.ui.swing.enums;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

import cdc.enums.EnumMask;
import cdc.enums.Mask;
import cdc.enums.MaskSupport;
import cdc.ui.swing.GridBagConstraintsBuilder;

public class MaskSelector<M extends Mask<M, V>, V> extends JPanel {
    private static final long serialVersionUID = 1L;
    private final MaskSupport<M, V> support;
    private final Style style;
    private final boolean horizontal;
    private final Predicate<V> filter;
    private final Function<V, String> valueToString;

    private final JPanel wValues = new JPanel();
    private final JPanel wControls;
    private final JButton wAll;
    private final JButton wNone;
    private final List<JToggleButton> wSelectors = new ArrayList<>();

    protected MaskSelector(MaskSupport<M, V> support,
                           Style style,
                           boolean horizontal,
                           boolean controls,
                           Predicate<V> filter,
                           Function<V, String> valueToString) {
        this.support = support;
        this.style = style;
        this.horizontal = horizontal;
        this.filter = filter;
        this.valueToString = valueToString;
        setLayout(new GridBagLayout());

        add(wValues,
            GridBagConstraintsBuilder.builder()
                                     .gridx(0)
                                     .gridy(0)
                                     .weightx(horizontal ? 1.0 : 0.0)
                                     .weighty(horizontal ? 0.0 : 1.0)
                                     .anchor(GridBagConstraints.LINE_START)
                                     .fill(GridBagConstraints.BOTH)
                                     .build());

        wValues.setLayout(new GridBagLayout());

        if (controls) {
            wControls = new JPanel();
            wControls.setLayout(new GridBagLayout());
            wAll = new JButton("All");
            wAll.setMargin(new Insets(0, 0, 0, 0));
            wNone = new JButton("None");
            wNone.setMargin(new Insets(0, 0, 0, 0));
            wControls.add(wAll,
                          GridBagConstraintsBuilder.builder()
                                                   .gridx(0)
                                                   .gridy(0)
                                                   .insets(0, 5, 0, 5)
                                                   .build());

            wControls.add(wNone,
                          GridBagConstraintsBuilder.builder()
                                                   .gridx(1)
                                                   .gridy(0)
                                                   .insets(0, 0, 0, 0)
                                                   .build());

            wAll.addActionListener(e -> selectAll());
            wNone.addActionListener(e -> deselectAll());

            add(wControls,
                GridBagConstraintsBuilder.builder()
                                         .gridx(horizontal ? 1 : 0)
                                         .gridy(horizontal ? 0 : 1)
                                         .build());

        } else {
            this.wControls = null;
            wAll = null;
            wNone = null;
        }

        build();

        support.getType().addEventHandler(e -> build());
    }

    public enum Style {
        CHECK_BOXES,
        TOGGLE_BUTTONS
    }

    private List<V> getVisibleValues() {
        return support.getType()
                      .getValues()
                      .stream()
                      .filter(filter)
                      .filter(support.getType()::isValid)
                      .toList();
    }

    private void build() {
        final List<V> newValues = getVisibleValues();
        if (support.isNullable() && filter.test(null)) {
            newValues.add(null);
        }
        final int newCount = newValues.size();

        final int dx = horizontal ? 1 : 0;
        final int dy = horizontal ? 0 : 1;

        // Create missing value components
        while (newCount > wSelectors.size()) {
            final int x = wSelectors.size() * dx;
            final int y = wSelectors.size() * dy;
            final JToggleButton wComponent;
            if (style == Style.TOGGLE_BUTTONS) {
                wComponent = new JToggleButton();
            } else {
                wComponent = new JCheckBox();
            }
            final int anchor = horizontal
                    ? GridBagConstraints.CENTER
                    : GridBagConstraints.LINE_START;
            wValues.add(wComponent,
                        GridBagConstraintsBuilder.builder()
                                                 .gridx(x)
                                                 .gridy(y)
                                                 .anchor(anchor)
                                                 .weightx(1.0)
                                                 .build());
            wSelectors.add(wComponent);
        }

        // Remove in excess value components
        while (newCount < wSelectors.size()) {
            final JComponent wComponent = wSelectors.get(wSelectors.size() - 1);
            wValues.remove(wComponent);
            wSelectors.remove(wSelectors.size() - 1);
        }

        // Update each component
        for (int index = 0; index < newValues.size(); index++) {
            final V value = newValues.get(index);
            final JToggleButton selector = wSelectors.get(index);
            selector.setText(valueToString.apply(value));
        }

        refresh();
    }

    private void refresh() {
        setMask(getMask());
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        for (final JToggleButton selector : wSelectors) {
            selector.setEnabled(enabled);
        }
        if (wAll != null) {
            wAll.setEnabled(enabled);
        }
        if (wNone != null) {
            wNone.setEnabled(enabled);
        }
    }

    public MaskSelector<M, V> selectAll() {
        for (final JToggleButton selector : wSelectors) {
            selector.setSelected(true);
        }
        return this;
    }

    public MaskSelector<M, V> deselectAll() {
        for (final JToggleButton selector : wSelectors) {
            selector.setSelected(false);
        }
        return this;
    }

    public MaskSelector<M, V> setMask(M mask) {
        final List<V> values = getVisibleValues();
        for (int index = 0; index < values.size(); index++) {
            final V value = values.get(index);
            final JToggleButton selector = wSelectors.get(index);
            selector.setSelected(mask.isSet(value));
        }
        return this;
    }

    public M getMask() {
        M mask = support.empty();
        final List<V> values = getVisibleValues();
        for (int index = 0; index < values.size(); index++) {
            final V value = values.get(index);
            final JToggleButton selector = wSelectors.get(index);
            if (selector.isSelected()) {
                mask = mask.or(value);
            }
        }
        return mask;
    }

    public static <M extends Mask<M, V>, V> Builder<M, V> builder(MaskSupport<M, V> support) {
        return new Builder<>(support);
    }

    public static <E extends Enum<E>> Builder<EnumMask<E>, E> builder(Class<E> enumClass) {
        return new Builder<>(EnumMask.support(enumClass));
    }

    public static class Builder<M extends Mask<M, V>, V> {
        private final MaskSupport<M, V> support;
        private Style style = Style.CHECK_BOXES;
        private boolean horizontal = false;
        private boolean controls = true;
        private Predicate<V> filter = v -> true;
        private Function<V, String> valueToString = Object::toString;

        protected Builder(MaskSupport<M, V> support) {
            this.support = support;
        }

        public Builder<M, V> style(Style style) {
            this.style = style;
            return this;
        }

        public Builder<M, V> horizontal() {
            this.horizontal = true;
            return this;
        }

        public Builder<M, V> vertical() {
            this.horizontal = false;
            return this;
        }

        public Builder<M, V> controls(boolean controls) {
            this.controls = controls;
            return this;
        }

        public Builder<M, V> filter(Predicate<V> filter) {
            this.filter = filter;
            return this;
        }

        public Builder<M, V> valueToString(Function<V, String> valueToString) {
            this.valueToString = valueToString;
            return this;
        }

        public MaskSelector<M, V> build() {
            return new MaskSelector<>(support,
                                      style,
                                      horizontal,
                                      controls,
                                      filter,
                                      valueToString);
        }
    }
}