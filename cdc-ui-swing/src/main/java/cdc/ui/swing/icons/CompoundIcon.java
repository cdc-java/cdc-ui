package cdc.ui.swing.icons;

import java.awt.Component;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.Icon;

import cdc.util.lang.Checks;

public class CompoundIcon implements Icon {
    private final int width;
    private final int height;
    private final List<Part> parts = new ArrayList<>();

    public static class Ref {
        private final double x;
        private final double y;

        public Ref(double x,
                   double y) {
            this.x = x;
            this.y = y;
        }

        public final double getX() {
            return x;
        }

        public final double getY() {
            return y;
        }

        public final double getX(double width) {
            return (width - 1.0) * x;
        }

        public final double getY(double height) {
            return (height - 1.0) * y;
        }
    }

    public static class Part {
        private final Icon icon;
        private final Ref ref0;
        private final Ref ref1;

        public Part(Icon icon,
                    Ref ref0,
                    Ref ref1) {
            Checks.isNotNull(icon, "icon");
            Checks.isNotNull(ref0, "ref0");
            Checks.isNotNull(ref1, "ref1");

            this.icon = icon;
            this.ref0 = ref0;
            this.ref1 = ref1;
        }

        public final Icon getIcon() {
            return icon;
        }

        public final Ref getRef0() {
            return ref0;
        }

        public final Ref getRef1() {
            return ref1;
        }
    }

    public CompoundIcon(int width,
                        int heght,
                        Part... parts) {
        this.width = width;
        this.height = heght;
        for (final Part part : parts) {
            this.parts.add(part);
        }
    }

    public CompoundIcon(Icon icon,
                        int xmargin,
                        int ymargin,
                        Part... parts) {
        Checks.isNotNull(icon, "icon");

        this.width = icon.getIconWidth() + 2 * xmargin;
        this.height = icon.getIconHeight() + 2 * ymargin;
        this.parts.add(new Part(icon, new Ref(0.5, 0.5), new Ref(0.5, 0.5)));
        for (final Part part : parts) {
            this.parts.add(part);
        }
    }

    public CompoundIcon(Icon icon,
                        int xmargin,
                        int ymargin,
                        Collection<Part> parts) {
        Checks.isNotNull(icon, "icon");

        this.width = icon.getIconWidth() + 2 * xmargin;
        this.height = icon.getIconHeight() + 2 * ymargin;
        this.parts.add(new Part(icon, new Ref(0.5, 0.5), new Ref(0.5, 0.5)));
        for (final Part part : parts) {
            this.parts.add(part);
        }
    }

    private static int getMin(int size0,
                              double r0,
                              int size1,
                              double r1) {
        return (int) (r0 * size0 - r1 * size1);
    }

    @Override
    public void paintIcon(Component c,
                          Graphics g,
                          int x,
                          int y) {
        for (final Part part : parts) {
            final int dx = getMin(width, part.getRef0().getX(), part.getIcon().getIconWidth(), part.getRef1().getX());
            final int dy = getMin(height, part.getRef0().getY(), part.getIcon().getIconHeight(), part.getRef1().getY());
            part.getIcon().paintIcon(c, g, x + dx, y + dy);
        }
    }

    @Override
    public final int getIconWidth() {
        return width;
    }

    @Override
    public final int getIconHeight() {
        return height;
    }
}