package cdc.ui.swing.icons;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.awt.image.VolatileImage;

import javax.swing.Icon;
import javax.swing.ImageIcon;

public final class IconUtils {
    private IconUtils() {
    }

    public static Image iconToImage(Icon icon) {
        if (icon instanceof ImageIcon) {
            return ((ImageIcon) icon).getImage();
        } else {
            final int width = icon.getIconWidth();
            final int height = icon.getIconHeight();
            final BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
            final Graphics2D g = image.createGraphics();
            icon.paintIcon(null, g, 0, 0);
            g.dispose();
            return image;
        }
    }

    public static BufferedImage iconToBufferedImage(Icon icon,
                                                    int imageType) {
        return imageToBufferedImage(iconToImage(icon), imageType);
    }

    public static BufferedImage imageToBufferedImage(Image image,
                                                     int imageType) {
        if (image instanceof BufferedImage) {
            return (BufferedImage) image;
        } else if (image instanceof VolatileImage) {
            return ((VolatileImage) image).getSnapshot();
        } else {
            loadImage(image);
            final BufferedImage result = new BufferedImage(image.getWidth(null), image.getHeight(null), imageType);
            final Graphics2D g2 = result.createGraphics();
            g2.drawImage(image, null, null);
            g2.dispose();
            return result;
        }
    }

    private static void loadImage(Image image) {
        class StatusObserver implements ImageObserver {
            boolean imageLoaded = false;

            @Override
            public boolean imageUpdate(final Image img,
                                       final int infoflags,
                                       final int x,
                                       final int y,
                                       final int width,
                                       final int height) {
                if (infoflags == ALLBITS) {
                    synchronized (this) {
                        imageLoaded = true;
                        notifyAll();
                    }
                    return true;
                }
                return false;
            }
        }
        final StatusObserver imageStatus = new StatusObserver();
        synchronized (imageStatus) {
            if (image.getWidth(imageStatus) == -1 || image.getHeight(imageStatus) == -1) {
                while (!imageStatus.imageLoaded) {
                    try {
                        imageStatus.wait();
                    } catch (final InterruptedException ex) {
                        // Ignore
                    }
                }
            }
        }
    }
}