package cdc.ui.swing.prefs;

import java.awt.Color;
import java.util.prefs.Preferences;

import cdc.prefs.AbstractPreference;

/**
 * Color preference (node, key) pair.
 *
 * @author Damien Carbonne
 */
public final class ColorPreference extends AbstractPreference<Color> {
    public ColorPreference(Preferences node,
                           String key,
                           Color def) {
        super(Color.class,
              node,
              key,
              def);
    }

    @Override
    protected String toString(Color value) {
        return colorToString(value);
    }

    @Override
    protected Color fromString(String s) {
        return stringToColor(s);
    }

    public static String colorToString(Color color) {
        final int a = color.getAlpha();
        final int r = color.getRed();
        final int g = color.getGreen();
        final int b = color.getBlue();
        final int code = ((a & 0xFF) << 24)
                | ((r & 0xFF) << 16)
                | ((g & 0xFF) << 8)
                | (b & 0xFF);

        return Integer.toHexString(code);
    }

    public static Color stringToColor(String s) {
        final int code = (int) Long.parseLong(s, 16);
        final int a = (code >> 24) & 0xFF;
        final int r = (code >> 16) & 0xFF;
        final int g = (code >> 8) & 0xFF;
        final int b = (code) & 0xFF;

        return new Color(r, g, b, a);
    }
}