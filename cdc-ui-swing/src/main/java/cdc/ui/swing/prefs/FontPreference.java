package cdc.ui.swing.prefs;

import java.awt.Font;
import java.util.prefs.Preferences;

import cdc.prefs.AbstractPreference;

/**
 * Font preference (node, key) pair.
 *
 * @author Damien Carbonne
 */
public final class FontPreference extends AbstractPreference<Font> {
    private static final String BOLD = "-bold";
    private static final String BOLD_ITALIC = "-bold-italic";
    private static final String ITALIC = "-italic";

    public FontPreference(Preferences node,
                          String key,
                          Font def) {
        super(Font.class,
              node,
              key,
              def);
    }

    @Override
    protected String toString(Font value) {
        return fontToString(value);
    }

    @Override
    protected Font fromString(String s) {
        return stringToFont(s);
    }

    /**
     * Converts a font to a string.
     * <p>
     * The result follows this pattern: {@code name[-bold][-italic]-size}
     *
     * @param font The font.
     *
     * @return A string representation of {@code font}.
     */
    public static String fontToString(Font font) {
        final String style;
        if (font.isBold()) {
            style = font.isItalic() ? BOLD_ITALIC : BOLD;
        } else {
            style = font.isItalic() ? ITALIC : "";
        }

        return font.getName() + style + "-" + font.getSize();
    }

    /**
     * Converts a string to a font.
     *
     * @param s The string. It must be compliant with expected format.
     * @return The font corresponding to {@code s}.
     */
    public static Font stringToFont(String s) {
        final int p = s.lastIndexOf('-');
        final int size = Integer.parseInt(s.substring(p + 1));

        final String sub = s.substring(0, p);
        final int style;
        final String name;
        if (sub.endsWith(BOLD_ITALIC)) {
            style = Font.BOLD | Font.ITALIC;
            name = sub.substring(0, sub.length() - BOLD_ITALIC.length());
        } else if (sub.endsWith(BOLD)) {
            style = Font.BOLD;
            name = sub.substring(0, sub.length() - BOLD.length());
        } else if (sub.endsWith(ITALIC)) {
            style = Font.ITALIC;
            name = sub.substring(0, sub.length() - ITALIC.length());
        } else {
            style = Font.PLAIN;
            name = sub;
        }
        return new Font(name, style, size);
    }
}