package cdc.ui.swing.progress;

import java.awt.Component;

import cdc.ui.swing.SwingUtils;
import cdc.util.events.ProgressController;
import cdc.util.events.ProgressEvent;
import cdc.util.events.ProgressEventFilter;

/**
 * Swing based progress controller.
 * <p>
 * It delegates work to a ProgressMonitor.
 *
 * @author Damien Carbonne
 *
 */
public class SwingProgressController implements ProgressController {
    private final SwingProgressMonitor monitor;
    private final ProgressEventFilter filter;

    public SwingProgressController(Component parentComponent,
                                   String message) {
        this.monitor = new SwingProgressMonitor(parentComponent, message, "");
        this.filter = new ProgressEventFilter(this::doOnProgress);
    }

    private void doOnProgress(ProgressEvent event) {
        SwingUtils.processInEDT(() -> {
            final StringBuilder builder = new StringBuilder();
            if (event.getDetail() != null) {
                builder.append(event.getDetail());
                if (event.getPercents() >= 0.0) {
                    builder.append(" ");
                }
            }
            if (event.getPercents() >= 0.0) {
                builder.append(String.format("%d%%", (int) event.getPercents()));
            }
            monitor.setNote(builder.toString());
            monitor.setProgress(event.getPercents());
        });
    }

    @Override
    public void onProgress(ProgressEvent event) {
        filter.notifyProgress(event);
    }

    @Override
    public boolean isCancelled() {
        return monitor.isCanceled();
    }

    public void setMinDeltaMillis(int millis) {
        filter.setMinDeltaMillis(millis);
    }

    public int getMinDeltaMillis() {
        return filter.getMinDeltaMillis();
    }

    public void setMinDeltaPercents(double percents) {
        filter.setMinDeltaPercents(percents);
    }

    public double getMinDeltaPercents() {
        return filter.getMinDeltaPercents();
    }

    public void setMillisToPopup(int millis) {
        monitor.setMillisToPopup(millis);
    }

    public int getMillisToPopup() {
        return monitor.getMillisToPopup();
    }

    public void setMillisToDecideToPopup(int millis) {
        monitor.setMillisToDecideToPopup(millis);
    }

    public int getMillisToDecideToPopup() {
        return monitor.getMillisToDecideToPopup();
    }
}