package cdc.ui.swing.progress;

import java.awt.Component;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.UIManager;

public class SwingProgressMonitor {
    private static final double MAX_OVER_100 = 10.0;
    private final Component wParentComponent;
    private final long t0;
    private final Object message;
    private String note;

    private final Object[] cancelOption = new Object[1];

    private int millisToDecideToPopup = 500;
    private int millisToPopup = 2000;

    private JDialog wDialog;
    private JOptionPane wOptionPane;
    private JProgressBar wProgressBar;
    private JLabel wNote;

    public SwingProgressMonitor(Component parentComponent,
                                Object message,
                                String note) {
        this.wParentComponent = parentComponent;
        this.t0 = System.currentTimeMillis();
        this.message = message;
        this.note = note;

        this.cancelOption[0] = UIManager.getString("OptionPane.cancelButtonText");
    }

    public void setMillisToDecideToPopup(int millisToDecideToPopup) {
        this.millisToDecideToPopup = millisToDecideToPopup;
    }

    public int getMillisToDecideToPopup() {
        return millisToDecideToPopup;
    }

    public void setMillisToPopup(int millisToPopup) {
        this.millisToPopup = millisToPopup;
    }

    public int getMillisToPopup() {
        return millisToPopup;
    }

    public Object getMessage() {
        return message;
    }

    public void setNote(String note) {
        this.note = note;
        if (wNote != null) {
            wNote.setText(note);
        }
    }

    public String getNote() {
        return note;
    }

    public void close() {
        if (wDialog != null) {
            wDialog.setVisible(false);
            wDialog.dispose();
            wDialog = null;
            wOptionPane = null;
            wProgressBar = null;
        }
    }

    public boolean isCanceled() {
        if (wOptionPane == null) {
            return false;
        }

        final Object v = wOptionPane.getValue();
        if (v == null) {
            return false;
        }

        return (v.equals(cancelOption[0]) ||
                v.equals(Integer.valueOf(JOptionPane.CLOSED_OPTION)));
    }

    private static int percentsToValue(double percents) {
        return (int) (percents * MAX_OVER_100);
    }

    public void setProgress(double percents) {
        if (percents >= 100.0) {
            close();
        } else if (wProgressBar != null) {
            if (percents < 0.0) {
                if (!wProgressBar.isIndeterminate()) {
                    wProgressBar.setIndeterminate(true);
                }
            } else {
                if (wProgressBar.isIndeterminate()) {
                    wProgressBar.setIndeterminate(false);
                }
                wProgressBar.setValue(percentsToValue(percents));
            }
        } else {
            final long t = System.currentTimeMillis();
            final int dt = (int) (t - t0);
            if (dt >= millisToDecideToPopup) {
                final int predictedCompletionTime;
                if (percents > 0.0) {
                    predictedCompletionTime = (int) (dt * 100.0 / percents);
                } else {
                    predictedCompletionTime = millisToPopup;
                }
                if (predictedCompletionTime >= millisToPopup) {
                    wProgressBar = new JProgressBar();
                    wProgressBar.setMinimum(0);
                    wProgressBar.setMaximum((int) (100.0 * MAX_OVER_100));
                    wProgressBar.setValue(percentsToValue(percents));
                    if (note != null) {
                        wNote = new JLabel(note);
                    }
                    wProgressBar.setIndeterminate(percents < 0.0);
                    wOptionPane = new ProgressOptionPane(new Object[] { message,
                            wNote,
                            wProgressBar });
                    wDialog = wOptionPane.createDialog(wParentComponent,
                                                       UIManager.getString("ProgressMonitor.progressText"));
                    wDialog.setVisible(true);
                }
            }
        }
    }

    private class ProgressOptionPane extends JOptionPane {
        private static final long serialVersionUID = 1L;

        public ProgressOptionPane(Object messageList) {
            super(messageList,
                  JOptionPane.INFORMATION_MESSAGE,
                  JOptionPane.DEFAULT_OPTION,
                  null,
                  SwingProgressMonitor.this.cancelOption,
                  null);
        }
    }
}