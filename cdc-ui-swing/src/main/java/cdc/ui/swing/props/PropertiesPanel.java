package cdc.ui.swing.props;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import cdc.ui.swing.GridBagConstraintsBuilder;

/**
 * Simple component used to align pairs of (name, component).
 *
 * @author Damien Carbonne
 */
public class PropertiesPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private int nextY = 0;
    private final List<JComponent> components = new ArrayList<>();

    public PropertiesPanel() {
        super();
        setLayout(new GridBagLayout());
    }

    public int addProperty(String name,
                           JComponent c,
                           boolean fill) {
        final JLabel label = new JLabel(name);
        components.add(label);
        add(label,
            GridBagConstraintsBuilder.builder()
                                     .gridx(0)
                                     .gridy(nextY)
                                     .weightx(0.0)
                                     .weighty(0.0)
                                     .insets(0, 0, 0, 5)
                                     .anchor(GridBagConstraints.LINE_START)
                                     .build());
        components.add(c);
        add(c,
            GridBagConstraintsBuilder.builder()
                                     .gridx(1)
                                     .gridy(nextY)
                                     .weightx(1.0)
                                     .weighty(0.0)
                                     .anchor(GridBagConstraints.LINE_START)
                                     .fill(fill ? GridBagConstraints.HORIZONTAL : GridBagConstraints.NONE)
                                     .build());

        nextY++;
        return getPropertiesCount() - 1;
    }

    public void addFiller() {
        final JPanel filler = new JPanel();
        filler.setPreferredSize(new Dimension(0, 0));
        add(filler,
            GridBagConstraintsBuilder.builder()
                                     .gridx(0)
                                     .gridwidth(2)
                                     .gridy(nextY)
                                     .weightx(1.0)
                                     .weighty(1.0)
                                     .fill(GridBagConstraints.BOTH)
                                     .build());
        nextY++;
    }

    public int getPropertiesCount() {
        return components.size() / 2;
    }

    public JLabel getLabelAt(int index) {
        return (JLabel) components.get(2 * index);
    }

    public String getNameAt(int index) {
        return getLabelAt(index).getText();
    }

    public JComponent getPropertyAt(int index) {
        return components.get(2 * index + 1);
    }

    public void setLabelToolTipTextAt(int index,
                                      String text) {
        getLabelAt(index).setToolTipText(text);
    }

    public void setPropertyToolTipTextAt(int index,
                                         String text) {
        getPropertyAt(index).setToolTipText(text);
    }
}