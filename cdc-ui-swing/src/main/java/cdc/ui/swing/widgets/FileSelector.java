package cdc.ui.swing.widgets;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;

import cdc.ui.swing.GridBagConstraintsBuilder;

public class FileSelector extends JPanel {
    private static final long serialVersionUID = 1L;

    private final Mode mode;
    private final JTextField wText = new JTextField();
    private final JButton wOpen = new JButton("...");

    public FileSelector(Mode mode) {
        this.mode = mode;
        setLayout(new GridBagLayout());
        wOpen.setMargin(new Insets(0, 0, 0, 0));

        add(wText,
            GridBagConstraintsBuilder.builder()
                                     .gridx(0)
                                     .gridy(0)
                                     .weightx(1.0)
                                     .fill(GridBagConstraints.HORIZONTAL)
                                     .build());
        add(wOpen,
            GridBagConstraintsBuilder.builder()
                                     .gridx(1)
                                     .gridy(0)
                                     .insets(0, 5, 0, 0)
                                     .fill(GridBagConstraints.BOTH)
                                     .build());

        wOpen.addActionListener(e -> selectFile());
    }

    public enum Mode {
        FILES_ONLY,
        DIRECTORIES_ONLY,
        FILES_AND_DIRECTORIES
    }

    public void setFile(File file) {
        if (file.exists()) {
            if (mode == Mode.DIRECTORIES_ONLY) {
                if (!file.isDirectory()) {
                    throw new IllegalArgumentException(file + " is not a directory");
                }
            } else if (mode == Mode.FILES_ONLY && file.isDirectory()) {
                throw new IllegalArgumentException(file + " is a directory");
            }
        }
        wText.setText(file.getPath());
    }

    public File getFile() {
        return new File(wText.getText());
    }

    private void selectFile() {
        final JFileChooser wChooser = new JFileChooser();
        wChooser.setMultiSelectionEnabled(false);
        if (mode == Mode.FILES_ONLY) {
            wChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        } else if (mode == Mode.DIRECTORIES_ONLY) {
            wChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        } else {
            wChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        }

        wChooser.setCurrentDirectory(getFile());
        final int result = wChooser.showOpenDialog(this);
        if (result == JFileChooser.APPROVE_OPTION) {
            setFile(wChooser.getSelectedFile());
        }
    }
}