package cdc.ui.swing.widgets;

import java.awt.Component;
import java.io.File;

import javax.swing.JFrame;

public class HtmlFrame extends JFrame {
    private static final long serialVersionUID = 1L;
    private final HtmlPanel wPanel = new HtmlPanel();

    public HtmlFrame(Component parentComponent) {
        setContentPane(wPanel);
        setLocationRelativeTo(parentComponent);
    }

    public HtmlPanel getHtmlPanel() {
        return wPanel;
    }

    public static void showFrame(Component parentComponent,
                                 String title,
                                 String message,
                                 File... files) {
        final HtmlFrame frame = new HtmlFrame(parentComponent);
        frame.setTitle(title);
        if (message != null) {
            frame.getHtmlPanel()
                 .append(title)
                 .append("<br><br>");
        }
        for (final File file : files) {
            frame.getHtmlPanel()
                 .href(file)
                 .append("<br>");
        }
        frame.getHtmlPanel()
             .build();
        frame.pack();
        frame.setResizable(false);
        frame.setVisible(true);
    }
}