package cdc.ui.swing.widgets;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.plaf.basic.BasicButtonUI;

public class TabCloseButton extends JPanel {
    private static final long serialVersionUID = 1L;
    private static final int MAX_TAB_WIDTH = 100;
    protected final JTabbedPane wOwner;
    private final JLabel wLabel = new JLabel();
    private final CloseButton wClose = new CloseButton();

    public TabCloseButton(JTabbedPane owner,
                          String title) {
        super(new FlowLayout(FlowLayout.LEFT, 0, 0));

        this.wOwner = owner;
        setOpaque(false);
        wLabel.setText(title);
        // We don't want the label to be too long
        final Dimension dim = wLabel.getPreferredSize();
        wLabel.setMaximumSize(new Dimension(MAX_TAB_WIDTH, dim.height));
        add(wLabel);

        wClose.setMargin(new Insets(0, 0, 0, 0));
        add(wClose);
    }

    public JLabel getLabel() {
        return wLabel;
    }

    private class CloseButton extends JButton implements ActionListener {
        private static final long serialVersionUID = 1L;

        public CloseButton() {
            final int size = 15;
            setPreferredSize(new Dimension(size, size));
            setToolTipText("Close");
            // Make the button looks the same for all Laf's
            setUI(new BasicButtonUI());
            setContentAreaFilled(false);
            setFocusable(false);
            setBorderPainted(false);
            setRolloverEnabled(true);
            // Close the proper tab by clicking the button
            addActionListener(this);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            final int index = wOwner.indexOfTabComponent(TabCloseButton.this);
            if (index == -1) {
                assert false;
            } else {
                wOwner.removeTabAt(index);
            }
        }

        // We don't want to update UI for this button
        @Override
        public void updateUI() {
            // Ignore
        }

        // Paint the cross
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            final Graphics2D g2 = (Graphics2D) g.create();
            // Shift the image for pressed buttons
            if (getModel().isPressed()) {
                g2.translate(1, 1);
            }
            g2.setStroke(new BasicStroke(2));
            g2.setColor(Color.BLACK);
            if (getModel().isRollover()) {
                g2.setColor(Color.RED);
            }
            final int delta = 3;
            g2.drawLine(delta, delta, getWidth() - delta - 1, getHeight() - delta - 1);
            g2.drawLine(getWidth() - delta - 1, delta, delta, getHeight() - delta - 1);
            g2.dispose();
        }
    }
}